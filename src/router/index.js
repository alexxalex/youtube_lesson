import Vue from 'vue'
import VueRouter from 'vue-router'
import firebase from 'firebase/app'

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    component: () => import('../layouts/MainLayout'),
    meta: { auth: true },
    children: [
      {
        path: '',
        component: () => import('../views/Home.vue')
      },
      {
        path: '/categories',
        component: () => import('../views/Categories.vue')
      },
      {
        path: '/detail/:id',
        component: () => import('../views/DetailRecord.vue')
      },
      {
        path: '/history',
        component: () => import('../views/History.vue')
      },
      {
        path: '/planning',
        component: () => import('../views/Planning.vue')
      },
      {
        path: '/profile',
        component: () => import('../views/Profile.vue')
      },
      {
        path: '/record',
        component: () => import('../views/Record.vue')
      }
    ]
  },
  {
    path: '/',
    component: () => import('../layouts/EmptyLayout'),
    children: [
      {
        path: '/login',
        component: () => import('../views/Login.vue')
      },
      {
        path: '/register',
        component: () => import('../views/Register.vue')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  const currentUser = firebase.auth().currentUser
  const requireAuth = to.matched.some(record => record.meta.auth)
  if (requireAuth && !currentUser) {
    next('/login?message=login')
  } else {
    next()
  }
})

export default router
