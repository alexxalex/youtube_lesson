import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import dateFilter from './filters/date.filter'
import currencyFilter from './filters/currency.filter'
import tooltipDirectives from '@/directives/tooltip.directive'
import messagePlugin from './utils/message.plugin'
import Loader from './components/app/Loader'
import 'materialize-css/dist/js/materialize.min'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false
Vue.use(messagePlugin)
Vue.use(Vuelidate)
Vue.filter('date', dateFilter)
Vue.filter('currency', currencyFilter)
Vue.directive('tooltip', tooltipDirectives)
Vue.component('Loader', Loader)

firebase.initializeApp({
  apiKey: "AIzaSyCWohObvS2SxAJj3FWLUvYcnLOfAGJvhSA",
  authDomain: "vue-less.firebaseapp.com",
  databaseURL: "https://vue-less.firebaseio.com",
  projectId: "vue-less",
  storageBucket: "vue-less.appspot.com",
  messagingSenderId: "813494333593",
  appId: "1:813494333593:web:bc887eba2c7934fd2d0808",
  measurementId: "G-D32NX9J2RH"
})

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
